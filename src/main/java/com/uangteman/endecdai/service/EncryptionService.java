package com.uangteman.endecdai.service;

import com.google.gson.Gson;
import com.uangteman.endecdai.constants.KeyAccess;
import com.uangteman.endecdai.constants.SecurityConstant;
import com.uangteman.endecdai.util.HttpUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Random;

@Service
public class EncryptionService {

    private static final Logger LOGGER = LogManager.getLogger(EncryptionService.class);

    public static String getStamp(KeyAccess keyAccess) {
        return String.valueOf(HttpUtil.session().getAttribute(keyAccess == KeyAccess.PRIVATE ? SecurityConstant.STAMP_PRIVATE : SecurityConstant.STAMP_PUBLIC));
    }

    /**
     * The functional is new generate stamp
     *
     * @param lengthCharacter this parameter used for length text or declare
     * @return result of parameter generate stamp
     */
    private static String generateStamp(int lengthCharacter) {
        try {

            int maxPosition = 5;
            int maxAlphabet = 2;

            Random rand = new Random();
            String output = null;
            String encrypt;

            Integer[] position = new Integer[maxPosition];
            int tempValue;

            char[] alphabetList = getRandAlphabet(maxAlphabet);

            // Genrate Position Random
            for (int i = 0; i < maxPosition; i++) {
                if (i > 1) {
                    tempValue = rand.nextInt(lengthCharacter - (position[0] + position[1]));
                    position[i] = getArrayIntegerNotDuplicate(position, tempValue, rand, lengthCharacter - (position[0] + position[1]));
                } else {
                    tempValue = rand.nextInt(10);
                    position[i] = getArrayIntegerNotDuplicate(position, tempValue, rand, 10);
                }
            }

            // Sort A-Z
            Arrays.sort(position);

            // Convert Array Int To String
            String convertPositionString = new Gson().toJson(position);
            encrypt = Base64.encodeBase64String(convertPositionString.getBytes());
            output = alphabetList[0] + encrypt + alphabetList[maxAlphabet - 1];

            return output;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    /**
     * The Method used for recrusive function check duplicate random value
     *
     * @param array      this parameter used for list array existing
     * @param value      this parameter used for new value
     * @param rand       this parameter used for random function
     * @param nextNumber this parameter used for length
     * @return result of value no duplicate random value
     */
    private static Integer getArrayIntegerNotDuplicate(Integer[] array, int value, Random rand, int nextNumber) {
        boolean isSameValue = Arrays.asList(array).contains(value);
        Integer output = value;
        if (isSameValue) {
            output = rand.nextInt(nextNumber);
            return getArrayIntegerNotDuplicate(array, output, rand, nextNumber);
        }
        return output;
    }

    /**
     * Generate Alphabet Random
     *
     * @param maxAlphabet this parameter used for maximum character
     * @return
     */
    private static char[] getRandAlphabet(int maxAlphabet) {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        char[] alphabetList = new char[maxAlphabet];
        Random rand = new Random();

        // Generate Alphabet Random
        for (int j = 0; j < maxAlphabet; j++) {
            alphabetList[j] = alphabet.charAt(rand.nextInt(alphabet.length()));
        }

        return alphabetList;
    }

    /**
     * DAI Encryption Mobile
     *
     * @param plaintText used for text
     * @return result for encryption data
     */
    public String doEncrypt(String plaintText, KeyAccess keyAccess) {

        StringBuilder output = new StringBuilder();
        String pattern = null;
        String getStamp = null;
        Integer[] stamp;

        try {

            if (keyAccess == KeyAccess.PUBLIC) {
                pattern = (String) HttpUtil.session().getAttribute(SecurityConstant.PATTERN_PUBLIC);
                getStamp = (String) HttpUtil.session().getAttribute(SecurityConstant.STAMP_PUBLIC);
            } else if (keyAccess == KeyAccess.PRIVATE) {
                pattern = (String) HttpUtil.session().getAttribute(SecurityConstant.PATTERN_PRIVATE);
                getStamp = (String) HttpUtil.session().getAttribute(SecurityConstant.STAMP_PRIVATE);
            }

            String encrypt = Base64.encodeBase64String(plaintText.getBytes());
            int maxEncrypt = encrypt.length();

            if (pattern == null) {
                pattern = encrypt;
            }

            // Generate Stamp if null prefs
            if (getStamp == null) {
                String generateStamp = generateStamp(pattern.length());
                HttpUtil.session().setAttribute(keyAccess == KeyAccess.PRIVATE ? SecurityConstant.STAMP_PRIVATE : SecurityConstant.STAMP_PUBLIC, generateStamp);
                stamp = DecryptionService.decryptStamp(generateStamp);
            } else {
                stamp = DecryptionService.decryptStamp(getStamp);
            }

            LOGGER.info("Stamp : " + new Gson().toJson(stamp));

            assert stamp != null;
            char[] alphabetList = getRandAlphabet(stamp.length);

            LOGGER.info("Karakter Alphabet : " + new Gson().toJson(alphabetList));

            output.append(encrypt);

            int count = 0;
            for (int i = 0; i < stamp.length && stamp[i] <= maxEncrypt; i++) {
                output.insert(stamp[i].intValue(), alphabetList[i]);
                count++;
            }

            output.insert(0, count);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return output.toString();
    }

}
