package com.uangteman.endecdai.service;

import com.google.gson.Gson;
import com.uangteman.endecdai.constants.KeyAccess;
import com.uangteman.endecdai.constants.SecurityConstant;
import com.uangteman.endecdai.util.HttpUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class DecryptionService {
    private static final Logger LOGGER = LogManager.getLogger(DecryptionService.class);

    /**
     * @param stampEncode this parameter used for stampEncode
     * @return result decrypt stamp
     */
    public static Integer[] decryptStamp(String stampEncode) {
        Integer[] output;
        try {
            String stamp = stampEncode.substring(1, stampEncode.length() - 1);
            String stampOutput = new String(Base64.decodeBase64(stamp));
            output = new Gson().fromJson(stampOutput, Integer[].class);

            return output;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * DAI Decryption Mobile
     *
     * @param chiperText used for encryption text
     * @return result for decrypt data
     */
    public String doDecrypt(String chiperText, KeyAccess keyAccess) {
        String pattern = null;
        String getStamp = null;
        String decode = chiperText.substring(1, chiperText.length());
        int maxIterasi = Integer.valueOf(chiperText.substring(0, 1));
        StringBuilder output = new StringBuilder(decode);

        try {

            if (keyAccess == KeyAccess.PUBLIC) {
                pattern = (String) HttpUtil.session().getAttribute(SecurityConstant.PATTERN_PUBLIC);
                getStamp = (String) HttpUtil.session().getAttribute(SecurityConstant.STAMP_PUBLIC);
            } else if (keyAccess == KeyAccess.PRIVATE) {
                pattern = (String) HttpUtil.session().getAttribute(SecurityConstant.PATTERN_PRIVATE);
                getStamp = (String) HttpUtil.session().getAttribute(SecurityConstant.STAMP_PRIVATE);
            }

            Integer[] stamp = decryptStamp(getStamp);
            int maxLength = chiperText.length();

            LOGGER.info("Result Enkrip : " + chiperText);

            for (int i = maxIterasi - 1; i >= 0; i--) {
                assert stamp != null;
                if (stamp[i] <= maxLength){
                    output.deleteCharAt(stamp[i]);
                }
            }

            LOGGER.info("Result Remove Character decrypt : " + output.toString());

            String decrypt = new String(Base64.decodeBase64(output.toString()));
            output.replace(0, output.length(), decrypt);


            LOGGER.info("Result decrypt : " + output.toString());
            return output.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }
}
