package com.uangteman.endecdai.Dto;

import lombok.Data;

@Data
public class RequestData {
    private String data;
    private String stamp;
}
