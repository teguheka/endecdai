package com.uangteman.endecdai.Dto;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class ResponseData<T> {

    @SerializedName("code")
    private int code = Integer.valueOf(0);

    @SerializedName("data")
    //@JsonAdapter(Base64TypeAdapterFactory.class)
    private T data = null;
}
