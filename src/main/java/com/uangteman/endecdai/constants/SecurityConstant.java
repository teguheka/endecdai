package com.uangteman.endecdai.constants;

public class SecurityConstant {
    // Web Service
    public static final String STAMP_PUBLIC = "security:stamp_public";
    public static final String PATTERN_PUBLIC = "security:pattern_public";


    //Local Storage, Preference, Cache on Sensitive Data
    public static final String STAMP_PRIVATE = "security:stamp_private";
    public static final String PATTERN_PRIVATE = "security:pattern_private";
}
