package com.uangteman.endecdai.controller;

import com.uangteman.endecdai.constants.KeyAccess;
import com.uangteman.endecdai.constants.SecurityConstant;
import com.uangteman.endecdai.service.EncryptionService;
import io.swagger.annotations.ApiParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/encrypt")
public class EncryptionController {
    private static final Logger LOGGER = LogManager.getLogger(EncryptionController.class);

    @Autowired
    private EncryptionService encryptionService;

    public static HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true); // true == allow create
    }

    @PostMapping("")
    public Map enrypt(@ApiParam(value = "{\"id_temp\":\"274704\",\"full_name\":\"Fajrin Testing\",\"gender\":\"M\",\"email_address\":\"jumintenayu008@gmail.com\",\"facebook_id\":\"\",\"gplus_id\":\"\",\"pob\":\"Kota Palembang\",\"dob\":\"19-12-1990\",\"religion\":1,\"education\":7,\"national_status\":\"WNI\",\"race_id\":1,\"personal_id_no\":\"5566336699788881\",\"mobile_no\":\"81212167580\",\"home_status\":1,\"dom_address\":\"jalan sini aja kali coy. gak usah kmana2\",\"telp_dom\":\"87946487540451\",\"dom_kelurahan\":4510,\"dom_kecamatan\":440,\"dom_kab_kot\":341,\"dom_province\":5,\"dom_postal_code\":4510,\"fam1_name\":\"suyqnto\",\"telp_fam1\":\"08794645787943431\",\"fam1_address\":\"jajsbaua aha aja aja a\",\"fam1_kelurahan\":4504,\"fam1_kecamatan\":439,\"fam1_kab_kot\":341,\"fam1_province\":5,\"fam1_postal_code\":4504,\"marital_status\":1,\"monthly_income\":8000000,\"hll_work\":\"2-5\",\"bank_name_id\":6,\"bank_username\":\"Fajrin Testing\",\"bank_number\":\"22233666444555111\",\"employer_type\":5,\"employer_name\":\"pt. apa aka lah\",\"employer_role\":\"haba aha aha aja \",\"employer_address\":\"haba aha aa aja a\",\"employer_kelurahan\":4433,\"employer_kecamatan\":428,\"employer_kab_kot\":172,\"employer_province\":5,\"employer_postal_code\":4433,\"telp_work\":\"0231870757846494\",\"start_created_at\":1482124137117,\"submit_at\":1482124548419,\"know_ut\":4,\"promo_code\":\"\",\"loan_amount\":3000000,\"loan_days_length\":30,\"loan_start_datetime\":1482124137117,\"loan_due_datetime\":1484629737117,\"loan_interest_fee\":1043547,\"loan_interest_rate\":1,\"loan_repay_amount\":4043547,\"loan_purpose\":4,\"dependents\":0,\"is_bankacc_correct\":\"Y\",\"prefix_telp_dom\":\"021\",\"main_expenses\":4000000,\"houseloan\":0}")
                      @RequestBody String data) {
        //ObjectMapper mapper = new ObjectMapper();
        //String userJson = mapper.writeValueAsString(data);
        String encrypted = encryptionService.doEncrypt(data, KeyAccess.PUBLIC);

        Map map = new HashMap();
        map.put("data", encrypted);
        map.put("stamp", session().getAttribute(SecurityConstant.STAMP_PUBLIC));
        return map;
    }
}
