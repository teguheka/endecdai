package com.uangteman.endecdai.controller;

import com.uangteman.endecdai.Dto.RequestData;
import com.uangteman.endecdai.constants.KeyAccess;
import com.uangteman.endecdai.constants.SecurityConstant;
import com.uangteman.endecdai.service.DecryptionService;
import com.uangteman.endecdai.util.HttpUtil;
import io.micrometer.core.instrument.util.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/decrypt")
public class DecryptionController {
    private static final Logger LOGGER = LogManager.getLogger(DecryptionController.class);

    @Autowired private DecryptionService decryptionService;

    @PostMapping("")
    public String decrypt(@RequestBody RequestData requestData) {
        if (StringUtils.isNotEmpty(requestData.getStamp())) {
            HttpUtil.session().setAttribute(SecurityConstant.STAMP_PUBLIC, requestData.getStamp());
        }
        //decrypt
        return decryptionService.doDecrypt(requestData.getData(), KeyAccess.PUBLIC);

        //Gson gson = new Gson();
        //String strJson = gson.toJson(tmpResponse);
        //ResponseData<List<User>> response = gson.fromJson(strJson, new TypeToken<ResponseData<List<User>>>() {}.getType());
    }
}
