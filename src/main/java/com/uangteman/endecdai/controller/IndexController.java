package com.uangteman.endecdai.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.GitProperties;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;


@RestController
@RequestMapping("/api/v1")
@Api(description = "Set of endpoints for Info session and host.")
public class IndexController {

    @Autowired private Environment environment;
    @Autowired(required = false) private GitProperties gitProperties;


    @GetMapping("/host")
    public Map<String, Object> hostInfo(HttpServletRequest request){
        Map<String, Object> info = new TreeMap<>();
        info.put("Hostname", request.getLocalName());
        info.put("IP Address Local", request.getLocalAddr());
        info.put("Port Local", request.getLocalPort());
        info.put("Active Profiles", environment.getActiveProfiles());
        if (gitProperties != null){
            info.put("Git Branch", gitProperties.getBranch());
            info.put("Git Commit ID", gitProperties.getCommitId());
            info.put("Git Commit Short Commit ID", gitProperties.getShortCommitId());
            info.put("Git Commit Time", gitProperties.getCommitTime());
        }
        return info;
    }

    @GetMapping("/session")
    public Map<String, Object> getSessionVariables(HttpSession session){
        Map<String, Object> sessionVariables = new TreeMap<>();
        Collections.list(session.getAttributeNames()).forEach(name -> {
            sessionVariables.put(name, session.getAttribute(name));
        });
        return sessionVariables;
    }

    @PostMapping("/session")
    @ResponseStatus(HttpStatus.CREATED)
    public void putSessionVariable(@RequestBody Map<String, String> data, HttpSession session){
        data.keySet().forEach(key -> session.setAttribute(key, data.get(key)));
    }

    @DeleteMapping("/session/{varname}")
    @ResponseStatus(HttpStatus.OK)
    public void putSessionVariable(@PathVariable String varname, HttpSession session){
        session.removeAttribute(varname);
    }
}
